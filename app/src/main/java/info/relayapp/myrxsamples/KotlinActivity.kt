package info.relayapp.myrxsamples

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class KotlinActivity : AppCompatActivity() {

    private var compositeDisposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin)

        sortFood()

        sortUsers()

    }

    private fun sortFood() {
        var disposable = Observable.just(getHealthyFood())
                .flatMap { strings -> Observable.fromIterable(strings) }
                .toSortedList()
                .subscribe({ strings ->
                    for (s in strings) {
                        Log.d("TAG", s)
                    }
                }) { throwable -> Log.d("TAG", throwable.localizedMessage) }

        compositeDisposables.add(disposable)
    }

    private fun sortUsers() {
        val disposable = Observable.just(getUsers())
                .flatMap { users -> Observable.fromIterable(users) }
                .toSortedList()
                .subscribe({ users ->
                    for (u in users) {
                        Log.d("SORTED USER", u.id.toString())
                    }
                }) { throwable -> Log.d("USER", throwable.localizedMessage) }

        compositeDisposables.add(disposable)
    }

    private fun getUsers(): java.util.ArrayList<User> {
        val user1 = User(3, "u3")
        val user2 = User(1, "u1")
        val user3 = User(2, "u2")

        val users = java.util.ArrayList<User>()
        users.add(user1)
        users.add(user2)
        users.add(user3)
        return users
    }

    private fun getHealthyFood(): List<String> {
        val healthyFood = ArrayList<String>()
        healthyFood.add("Protein")
        healthyFood.add("Apple")
        healthyFood.add("Low fat cheese")
        return healthyFood
    }

}
