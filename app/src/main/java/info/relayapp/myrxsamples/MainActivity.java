package info.relayapp.myrxsamples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Timed;

public class MainActivity extends AppCompatActivity {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        flatMapOperator();

        switchIfEmptyOperator();

        mapOperator();

        filterOperator();

        fromArrayOperator();

        mergeOperator();

        onErrorResumeOperator("User123");

        deferOperator();

        firstOperator();

        lastOperator();

        takeOperator();

        intervalOperator();

        concatOperator();

        skipOperator();

        reduceOperator();

        zipOperator();

    }

    private void zipOperator() {
        Observable healthyObseravable = Observable.just(getHealthyFood());
        Observable unhealtyhObservable = Observable.just(getUnhealthyFood());
        Disposable disposable = Observable.zip(healthyObseravable, unhealtyhObservable,
                (BiFunction<List<String>, List<String>, List<String>>) (t1, t2) -> {
                    t1.addAll(t2);
                    return t1;
                }).subscribe(o -> Log.d("ZIP", o.toString()), new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Log.d("ZIP", throwable.getLocalizedMessage());
            }
        });
        compositeDisposable.add(disposable);
    }

    /*
        Reduce apply function to each item emitted by an observable sequentially and then emmit the final value
        Reduce operator is often called accumulate, aggregate or compress.
     */

    private void reduceOperator() {
        Disposable disposable = Observable.just( 1, 2, 3, 4, 5)
                .reduce((integer, integer2) -> integer - integer2)
                .subscribe(number -> Log.d("REDUCE", String.valueOf(number)),
                        throwable -> Log.d("REDUCE", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);

    }

    private void skipOperator(){
        Disposable disposable = Observable.just(0, 1, 2, 3, 4, 5)
                .skip(1)
                .subscribe(s -> Log.d("SKIP", String.valueOf(s)),
                        throwable -> Log.d("SKIP", throwable.getLocalizedMessage()));
        compositeDisposable.add(disposable);
    }

    private void concatOperator() {
        Observable healtyhObseravable = Observable.just(getHealthyFood());
        Observable unhealtyObseravable =  Observable.just(getUnhealthyFood());

        Disposable disposable = Observable.concat(healtyhObseravable, unhealtyObseravable)
                .subscribe(o -> Log.d("SKIP", (String) o), new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("CONCAT", throwable.getLocalizedMessage());
                    }
                });

        compositeDisposable.add(disposable);
    }

    private void intervalOperator() {
        Disposable disposable = Observable.interval(1, TimeUnit.SECONDS)
                .subscribe(aLong -> {
                    Log.d("INTERVAL", aLong.toString());
                }, throwable -> Log.d("INTERVAL", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);
    }

    private void flatMapOperator() {
        Disposable disposable = Observable.just(getHealthyFood())
                .flatMap(strings -> Observable.fromIterable(strings))
                .subscribe(s -> Log.d("FLAT MAP", s),
                        throwable -> Log.d("FLAT MAP", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);
    }

    private void takeOperator(){
        Disposable disposable = Observable.just(getHealthyFood())
                .flatMap(strings -> Observable.fromIterable(strings))
                .take(2)
                .subscribe(s -> Log.d("TAKE", s), throwable -> Log.d("TAKE", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);
    }

    private void lastOperator(){
        Disposable disposable = Observable.just(getHealthyFood())
                .flatMap(strings -> Observable.fromIterable(strings))
                .lastOrError()
                .subscribe(s -> Log.d("LAST", s), throwable -> Log.d("LAST", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);
    }

    private void firstOperator(){
        Disposable disposable = Observable.just(getHealthyFood())
                .flatMap(strings -> Observable.fromIterable(strings))
                .firstOrError()
                .subscribe(s -> Log.d("FIRST", s), throwable -> Log.d("FIRST", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);
    }

    /*
    Defer - means postpone or delay.
    The defer operator waits until an observer subscribes to it and then it generates an Observable.
    Link: http://reactivex.io/documentation/operators/defer.html
     */
    private void deferOperator() {
        Disposable d = Observable.defer(() -> Observable.just(getHealthyFood()))
                .subscribe(strings -> printStringValues(strings, "DEFER"),
                        throwable -> Log.d("TAG", throwable.getLocalizedMessage()));

        compositeDisposable.add(d);
    }

    private void switchIfEmptyOperator() {
        Maybe foodMaybe = Maybe.just(getHealthyFood());

        Disposable disposable = getEmptyList()
                .switchIfEmpty(foodMaybe)
                .subscribe(new Consumer<List<String>>() {
                    @Override
                    public void accept(List<String> strings) throws Exception {
                        printStringValues(strings, "SWITCH IF EMPTY");
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("TAG", throwable.getLocalizedMessage());
                    }
                });

        compositeDisposable.add(disposable);
    }

    private void mapOperator(){
        Disposable disposable = Observable.just(getListOfNumbers())
                .flatMap(integers -> Observable.fromIterable(integers))
                .map(integer -> integer * 10)
                .toSortedList()
                .subscribe(integers -> printIntValues(integers, "MAP MULTIPLY: "),
                        throwable -> Log.d("TAG", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);
    }

    private void fromArrayOperator(){
        List<String > healthyFood = getHealthyFood();
        Disposable disposable = Observable.fromArray(healthyFood)
                .subscribe(strings -> printStringValues(strings, "FROM ARRAY"),
                        throwable -> Log.d("TAG", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);
    }

    private void mergeOperator() {
        Observable<List<String>> healthyObservable = Observable.just(getHealthyFood());
        Observable<List<String>> unhealthyObservable = Observable.just(getUnhealthyFood());

        Disposable disposable  = Observable.merge(healthyObservable, unhealthyObservable)
                .subscribe(strings -> printStringValues(strings, "MERGE"),
                        throwable -> Log.d("TAG", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);
    }

    private void filterOperator(){
        simpleFilter();
        filerListOfNumbers();
    }

    private void filerListOfNumbers() {
        filterNumbersReactiveApproach();
        filterNumbersImperativeApproach();
    }

    private void filterNumbersReactiveApproach() {
        Disposable disposable = Observable.just(getListOfNumbers())
                .flatMap(ints -> Observable.fromIterable(ints))
                .filter(integer -> integer % 2 == 1)
                .toList()
                .subscribe(integers -> printIntValues(integers, "FILTER LIST"),
                        throwable -> Log.d("TAG", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);
    }

    private void filterNumbersImperativeApproach() {
        List<Integer> integers = new ArrayList<>();
        for(int i=0; i<getListOfNumbers().size();i++){
            if(getListOfNumbers().get(i) % 2 == 1){
                integers.add(getListOfNumbers().get(i));
            }
        }

        for(int j=0;j<integers.size();j++){
            Log.d("TAG", "FILTER LIST OO: "+integers.get(j));
        }
    }

    private void simpleFilter() {
        Disposable disposable = Observable.just(1,2,3,4,5, 6, 7, 8, 9)
                .filter(integer -> {
                    if(integer % 2 == 0){
                        return true;
                    }
                    return false;
                })
                .toList()
                .subscribe(integers -> printIntValues(integers, "SIMPLE FILTER"),
                        throwable -> Log.d("TAG", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);
    }

    private void onErrorResumeOperator(final String userName){
        List<User> users = new ArrayList<>();
        users.add(new User(1, "User1"));
        users.add(new User(2, "User2"));
        Disposable disposable = Single.just(users)
                .map(usersList -> {
                    for(User u : usersList){
                        if(u.getName().equals(userName)){
                            return u;
                        }
                    }
                    return Single.error(new NoSuchElementException());
                })
                .onErrorResumeNext(throwable -> {
                    if(throwable instanceof NoSuchElementException){
                        User marko = new User(1, "User1");
                        return Single.just(marko);
                    }
                    return Single.error(throwable);
                })
                .subscribe(o -> {
                    if(o instanceof User){
                        Log.d("ON ERROR RESUME USER:", ((User) o).getName());
                    } else {
                        Log.d("ON ERROR RESUME: ", o.toString());
                    }
                }, throwable -> Log.d("ON ERROR RESUME: ", throwable.getLocalizedMessage()));

        compositeDisposable.add(disposable);
    }

    private void printStringValues(List<String> strings, String tag) {
        for(String s : strings){
            Log.d(tag, s);
        }
    }

    private void printIntValues(List<Integer> integers, String tag){
        for(int i: integers){
            Log.d(tag, String.valueOf(i));
        }
    }

    private List<Integer> getListOfNumbers(){
        List<Integer> numbers = new ArrayList<>();
        for(int i=0;i<10;i++){
            numbers.add(i);
        }
        return numbers;
    }

    private Maybe<ArrayList<String>> getEmptyList(){
        return Maybe.empty();
    }

    private List<String> getHealthyFood() {
        List<String> healthyFood = new ArrayList<>();
        healthyFood.add("Protein");
        healthyFood.add("Apple");
        healthyFood.add("Low fat cheese");
        return healthyFood;
    }

    private List<String> getUnhealthyFood(){
        List<String> unhealthyFood = new ArrayList<>();
        unhealthyFood.add("Burger");
        unhealthyFood.add("Pizza");
        unhealthyFood.add("Cevapi");
        return unhealthyFood;
    }
}