package info.relayapp.myrxsamples;

import android.support.annotation.NonNull;

/**
 * Created by Zoran Pavlovic (zoranpavlovic.net) on 16/03/2018.
 */

public class User implements Comparable<User>{

    int id;
    String name;

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(@NonNull User user) {
        return this.id < user.id ? -1 : this.id == user.id ? 0 : 1;
    }
}
